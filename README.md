# Uma

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0)

> **uma** proper noun  
> u·ma \ ˈ(y)ümə\
> 
> Definition of *uma* (Entry 1 of 1)  
> 1 capitalised : of, relating to, or suggesting a celestial body  
> // the clue for seven down is celestial body, and he wrote *Uma* Thurman

This `Unity3D` web app visualises the surface of the planet (any, pick one) overlayed with any location data.